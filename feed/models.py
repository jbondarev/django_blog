from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
# Create your models here.

"""
    Модель статьи
"""
class Article(models.Model):
    STATUS_CHOICES = (
        ('draft', 'Draft'),
        ('published', 'Published')
    )
    title: models.CharField = models.CharField(max_length=255)
    slug: models.SlugField = models.SlugField(max_length=250,
                                              unique_for_date='publish')
    author: models.ForeignKey = models.ForeignKey(User, on_delete=models.CASCADE, related_name='blog_articles')
    body: models.TextField = models.TextField()
    publish: models.DateTimeField = models.DateTimeField(default=timezone.now)
    created: models.DateTimeField = models.DateTimeField(auto_now_add=True)
    updated: models.DateTimeField = models.DateTimeField(auto_now=True)
    status: models.DateTimeField = models.CharField(max_length=10,
                                                    choices=STATUS_CHOICES,
                                                    default='draft')
    class Meta:
        ordering = ('-publish',)
        
    def __repr__(self) -> str:
        return f"Article({self.title}, {self.slug})"